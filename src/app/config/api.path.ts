export enum ApiPath {
    Account = '/accounts',
    Product = '/products',
    ProductImages = '/images',
    Collection = '/collections'
}