import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { MessageService } from 'primeng/api';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { Product } from '../models/product.model';

@Injectable({
  providedIn: 'root',
})
export class ProductService {

  public productPath: string = 'http://localhost:8080/products';

  constructor(private http: HttpClient) { }

  getProducts(): Observable<Product[]> {
    return this.http.get<Product[]>(this.productPath);
  }

  getProductById(productId: number): Observable<Product> {
    const path = this.productPath + '/' + productId
    console.log(`making a request to ${path}`)
    return this.http.get<Product>(this.productPath + '/' + productId);
  }

  create(product: Product): Observable<Product> {
    return this.http.post<Product>(this.productPath, product);
  }

  update(productId: number, product: Product): Observable<Product> {
    throw new Error('Method not implemented.');
  }

  save(product: Product): Observable<Product> {
    throw new Error('Method not implemented.');
  }
}
