import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { of } from 'rxjs/internal/observable/of';
import { catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment'
import { ApiPath } from '../config/api.path';

@Injectable({
  providedIn: 'root',
})
export class ProductImagesService {

  public baseUrl = environment.baseUrl+"/"+ApiPath.ProductImages;

  constructor(private http: HttpClient) { }

  productImagesPath(productId: number): string {
    return `${this.baseUrl}/products/${productId}`;
  }

  uploadImage(image: any, productId: number): Observable<any> {
    console.log(`Uploading ${image} to ${this.productImagesPath(productId)}`);

    return this.http
      .post(this.productImagesPath(productId), image)
      .pipe(catchError(this.handleError<any[]>('uploadImages', [])));
  }

  getImagesInformation(productId: number): Observable<any> {
    console.log(`Fetching images information for ${productId}`);

    return this.http
      .get(this.productImagesPath(productId))
      .pipe(catchError(this.handleError<any[]>('getImagesInformation', [])));
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
