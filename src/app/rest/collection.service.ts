import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Collection } from '../models/collection.model';

@Injectable({
  providedIn: 'root'
})
export class CollectionService {

  COLLECTION_BASE_URL = "http://localhost:8080/collections"

  constructor(private http: HttpClient) { 

  }

  getAllCollections(): Observable<Collection[]>{
    return this.http.get<Collection[]>(this.COLLECTION_BASE_URL);
  }

  
}
