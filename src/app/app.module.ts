import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { AccordionModule } from 'primeng/accordion'; //accordion and accordion tab
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { FormsModule } from '@angular/forms';
import { MenubarModule } from 'primeng/menubar';
import { ProductsComponent } from './products/products.component';
import { CarouselModule } from 'primeng/carousel';
import { DataViewModule } from 'primeng/dataview';
import { HttpClientModule } from '@angular/common/http';
import { CardModule } from 'primeng/card';
import { HomeComponent } from './home/home.component';
import { FileUploadModule } from 'primeng/fileupload';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MessageService, SharedModule } from 'primeng/api';
import { MenuComponent } from './menu/menu.component';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { CollectionComponent } from './collection/collection.component';
import {CheckboxModule} from 'primeng/checkbox';

@NgModule({
  declarations: [AppComponent, 
    ProductDetailComponent, ProductsComponent, HomeComponent, MenuComponent, CollectionComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    AccordionModule,
    ButtonModule,
    InputTextModule,
    MenubarModule,
    CarouselModule,
    DataViewModule,
    SharedModule,
    CardModule,
    FileUploadModule,
    BrowserAnimationsModule,
    MessagesModule,
    MessageModule,
    CheckboxModule
  ],
  providers: [MessageService],
  bootstrap: [AppComponent],
})
export class AppModule { }
