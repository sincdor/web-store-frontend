export interface Product {
    collections: number[];
    createdAt: Date;
    description: string;
    id: number;
    lastUpdate: Date;
    name: string;
    ref: string;
    unitPrice: number;
    imagesId: number[];
} 