import { Component, OnInit } from '@angular/core';
import { Product } from '../models/product.model';
import { ProductImagesService } from '../rest/product-images.service';
import { ProductService } from '../rest/product.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css'],
})
export class ProductsComponent implements OnInit {
  products: Product[];
  imageBaseUrl: string;

  public static path = { path: 'products', component: ProductsComponent }

  constructor(
    private productService: ProductService,
    private productImageService: ProductImagesService) {
      this.imageBaseUrl = productImageService.baseUrl;
    }

  ngOnInit(): void {
    this.getProducts();
  }

  getProducts(): void {
    this.productService.getProducts().subscribe((result) => {
      console.log(`Get Product result returned ${result}`);
      this.products = result;
      console.log(this.products);
    });
  }
}
