import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Message, MessageService } from 'primeng/api';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Collection } from '../models/collection.model';
import { Product } from '../models/product.model';
import { CollectionService } from '../rest/collection.service';
import { ProductImagesService } from '../rest/product-images.service';
import { ProductService } from '../rest/product.service';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css'],
})
export class ProductDetailComponent implements OnInit {

  @Input()
  product: Product;
  collections: Collection[];

  uploadedFiles: any[] = [];

  imagesInfo: any[] = [];

  uploadPath = "";
  baseImageUrl = "";

  public static path = {
    path: 'detail/:productId',
    component: ProductDetailComponent,
  };

  constructor(
    private route: ActivatedRoute,
    private productService: ProductService,
    private productImagesService: ProductImagesService,
    private messageService: MessageService,
    private collectionService: CollectionService
  ) { 
    this.baseImageUrl = productImagesService.baseUrl;
  }

  ngOnInit(): void {
    this.getProduct();
    this.getCollections();
  }

  getProduct() {
    const productId: number = +this.route.snapshot.paramMap.get('productId');

    if (productId === 0) {
      this.product = {} as Product;
    } else {
      this.productService
        .getProductById(productId)
        .subscribe((product) => {
          console.log(product);
          if (product) {
            this.product = product;
            this.uploadPath = this.productImagesService.productImagesPath(this.product.id);
          } else {
          }
        });

      this.productImagesService.getImagesInformation(productId).subscribe(imagesInfo => {
        if (imagesInfo) {
          this.imagesInfo = imagesInfo;
        }
      })
    }

  }

  getCollections() {
    this.collectionService.getAllCollections()
      .subscribe(collections => {
        if (collections) {
          this.collections = collections;
          console.log(`${collections.length} collections fetched`);
        } else {
          console.log("Something when wrong while fetching the collections");
        }
      });
  }

  onSave(): void {
    console.log(`Saving ${this.product.name}...`);
    this.messageService.add({ severity: 'info', summary: `Saving ${this.product.name}...` })
    if (this.isNew(this.product)) {
      console.log("Creating a new Product");
      this.productService.create(this.product)
        .subscribe(
          (product) => {
            if (product) {
              console.log(`Created Product ${product}`);
              this.messageService.add({ severity: 'success', summary: `Product saved successfully` })
              this.product = product;
            }
          },
          (error) => {
            console.error(error);
            if (error.error.errors) {
              error.error.errors.forEach(err => {
                this.messageService.add({ severity: 'error', summary: err })
              });
            }
          });
    } else {
      this.productService.update(this.product.id, this.product).subscribe(result => {
        if (result) {
          this.product = result;
        }
      });
    }
  }

  isNew(product: Product): boolean {
    return product.id === 0 || product.id === undefined;
  }

  handleError(error: Error) {
    this.messageService.add({ severity: 'error', summary: error.message })
    return throwError(error.message);
  }
}
