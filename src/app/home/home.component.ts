import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public static path = { path: 'home', component: HomeComponent }

  constructor() { }

  ngOnInit(): void {
  }

}
